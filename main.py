#!/usr/bin/python3

"""
Copyright (C) 2015, 2016 Corentin Bocquillon <corentin+framagit@nybble.fr>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import tkinter as tk
from tkinter import filedialog
from random import randrange
import serial
import time

# arduino = serial.Serial("/dev/ttyUSB0", 9600)


class Window(tk.Frame):
    def __init__(self, master=None):
        tk.Frame.__init__(self, master)
        self.createWidgets(master)

    def createWidgets(self, master):
        self.squares = []
        self.squaresState = []
        self.canvas = tk.Canvas(master, width = 400, height=400)
        self.canvas.pack()
        self.canvas.bind("<Button-1>", self.events)
        self.createRectangles()

        tk.Button(master, text="Ouvrir", command=self.open).pack(side=tk.LEFT)
        tk.Button(master, text="Enregistrer", command=self.save).pack(side=tk.LEFT)
        tk.Button(master, text="RAZ", command=self.RAZ).pack(side=tk.LEFT)
        tk.Button(master, text="Reverse", command=self.reverse).pack(side=tk.LEFT)
        tk.Button(master, text="Random", command=self.fillRandomly).pack(side=tk.LEFT)
        tk.Button(master, text="Upload", command=self.upload).pack(side=tk.LEFT)

    def createRectangles(self):
        for i in range(8):
            for j in range(8):
                self.squares.append(
                    self.canvas.create_rectangle(0 + 50*j,
                                                 0 + i*50,
                                                 50 + 50*j,
                                                 50 + i*50,
                                                 fill="white"))
                self.squaresState.append(False)

    def events(self, event):
        row = int(event.y / 50)
        col = int(event.x / 50)
        number = 8 * row + col
        if self.squaresState[number] == False:
            self.canvas.itemconfig(self.squares[number], fill="black")
            self.squaresState[number] = True
        else:
            self.canvas.itemconfig(self.squares[number], fill="white")
            self.squaresState[number] = False


    def fillWithAList(self, list):
        for i in range(64):
            if list[i] == False:
                self.canvas.itemconfig(self.squares[i], fill="white")
                self.squaresState[i] = False
            else:
                self.canvas.itemconfig(self.squares[i], fill="black")
                self.squaresState[i] = True

    def RAZ(self):
        list = []
        for i in range(64):
            list.append(False)
        self.fillWithAList(list)

    def reverse(self):
        list = []
        for i in range(64):
            if self.squaresState[i] is False:
                list.append(True)
            else:
                list.append(False)
        self.fillWithAList(list)

    def fillRandomly(self):
        list = []
        for i in range(64):
            rand = randrange(2)
            if rand == 0:
                list.append(False)
            else:
                list.append(True)
        self.fillWithAList(list)

    def open(self):
        path = filedialog.askopenfilename()
        if path == "":
            return
        f = open(path, 'r')
        data = f.readline()

        list = []
        for i in range(64):
            if data[i] == "0":
                list.append(False)
            else:
                list.append(True)
        self.fillWithAList(list)

    def save(self):
        path = filedialog.asksaveasfilename()
        if path == "":
            return
        f = open(path, 'w')
        str = ""
        for element in self.squaresState:
            if element == False:
                str += "0"
            else:
                str += "1"

        f.write(str + '\n')

    def upload(self):

        
        list = []
        string=""
        for i in range(8):
            list.append(chr(65+i))

        for i in range(8):
            string = ""
            for j in range(8):
                if self.squaresState[i*8+j] == False:
                    string += "0"
                else:
                    string += "1"

            list[i] += str(versDecimal(2, string))
            print (list[i])
            arduino.write((list[i]).encode("utf-8"))

def versDecimal(base, nombre):
    nombreEnDecimal = 0

    for i in range(len(nombre)):
        nombreEnDecimal += int(symboleVersNombre(nombre[len(nombre)-1-i])) * base ** i

    return nombreEnDecimal

def symboleVersNombre(symbole):
    dict = {}

    for i in range(10):
        dict[str(i)] = i

    for i in range(6):
        dict[chr(ord('A') + i)] = 10+i
        
    return dict[symbole]


root = tk.Tk()
window = Window(master=root)
window.mainloop()
